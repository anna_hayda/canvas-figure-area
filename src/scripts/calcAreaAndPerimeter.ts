import { CoordsArray } from './model.js';

const cmInPixel: number = 0.0264583333;

export const getArea = (coords: CoordsArray): number => {
    let area: number = 0;
    let j: number = coords.length - 1;
    for (let i: number = 0; i < coords.length; i++) {
        area += coords[i].x * coords[j].y - coords[i].y * coords[j].x;
        j = i;
    }
    area = Math.abs(area / 2);
    return area * cmInPixel;
}

export const getPerimeter = (coords: CoordsArray): number => {
    const coordsCopy: CoordsArray = [...coords];
    coordsCopy.push(coordsCopy[0]);
    let perimeter: number = 0;
    for (let k: number = 0; k < coordsCopy.length - 1; k++) {
        let xDiff: number = coordsCopy[k + 1].x - coordsCopy[k].x;
        let yDiff: number = coordsCopy[k + 1].y - coordsCopy[k].y;
        perimeter = perimeter + Math.pow(xDiff * xDiff + yDiff * yDiff, 0.5);
    }
    return perimeter * cmInPixel;
}
