import { getArea, getPerimeter } from './calcAreaAndPerimeter.js';
import { Coords, CoordsArray } from './model.js';

const coordsArr: CoordsArray = [];
const canvas: HTMLCanvasElement = document.getElementById('canvas') as HTMLCanvasElement;
const context: CanvasRenderingContext2D = canvas.getContext('2d');
let mouseLeave: boolean = false;
let coordinates: Coords;


canvas.addEventListener('click', (event: MouseEvent) => {
    coordinates = {x: event.x, y: event.y};
    coordsArr.push(coordinates);
    const max: number = coordsArr.length - 1;
    if (coordsArr[max - 1] !== undefined) {
        const curr: Coords = coordsArr[max];
        const prev: Coords = coordsArr[max - 1];
        context.beginPath();
        context.moveTo(prev.x, prev.y);
        context.lineTo(curr.x, curr.y);
        context.stroke();
    }
});

canvas.addEventListener('mouseleave', () => {
    if (coordsArr.length > 1) {
        context.lineTo(coordsArr[0].x, coordsArr[0].y);
        context.stroke();
        mouseLeave = true;
    }
});

document.getElementById('calcButton').addEventListener('click', () => {
    if (coordsArr.length > 2) {
        document.getElementById('area').innerHTML = `Area: ${getArea(coordsArr).toFixed(2)} cm<sup>2</sup>`;
        document.getElementById('perimeter').innerText = `Perimeter: ${getPerimeter(coordsArr).toFixed(2)} cm`;
    } else {
        alert('Must be at least 3 coordinates');
    }
});
