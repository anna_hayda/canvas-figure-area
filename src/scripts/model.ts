export interface Coords {
    x: number,
    y: number
}

export type CoordsArray = Array<Coords>;
