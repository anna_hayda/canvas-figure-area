import { getArea, getPerimeter } from './calcAreaAndPerimeter.js';
const coordsArr = [];
const canvas = document.getElementById('canvas');
const context = canvas.getContext('2d');
let mouseLeave = false;
let coordinates;
canvas.addEventListener('click', (event) => {
    coordinates = { x: event.x, y: event.y };
    coordsArr.push(coordinates);
    const max = coordsArr.length - 1;
    if (coordsArr[max - 1] !== undefined) {
        const curr = coordsArr[max];
        const prev = coordsArr[max - 1];
        context.beginPath();
        context.moveTo(prev.x, prev.y);
        context.lineTo(curr.x, curr.y);
        context.stroke();
    }
});
canvas.addEventListener('mouseleave', () => {
    if (coordsArr.length > 1) {
        context.lineTo(coordsArr[0].x, coordsArr[0].y);
        context.stroke();
        mouseLeave = true;
    }
});
document.getElementById('calcButton').addEventListener('click', () => {
    if (coordsArr.length > 2) {
        document.getElementById('area').innerHTML = `Area: ${getArea(coordsArr).toFixed(2)} cm<sup>2</sup>`;
        document.getElementById('perimeter').innerText = `Perimeter: ${getPerimeter(coordsArr).toFixed(2)} cm`;
    }
    else {
        alert('Must be at least 3 coordinates');
    }
});
