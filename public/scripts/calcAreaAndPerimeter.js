const cmInPixel = 0.0264583333;
export const getArea = (coords) => {
    let area = 0;
    let j = coords.length - 1;
    for (let i = 0; i < coords.length; i++) {
        area += coords[i].x * coords[j].y - coords[i].y * coords[j].x;
        j = i;
    }
    area = Math.abs(area / 2);
    return area * cmInPixel;
};
export const getPerimeter = (coords) => {
    const coordsCopy = [...coords];
    coordsCopy.push(coordsCopy[0]);
    let perimeter = 0;
    for (let k = 0; k < coordsCopy.length - 1; k++) {
        let xDiff = coordsCopy[k + 1].x - coordsCopy[k].x;
        let yDiff = coordsCopy[k + 1].y - coordsCopy[k].y;
        perimeter = perimeter + Math.pow(xDiff * xDiff + yDiff * yDiff, 0.5);
    }
    return perimeter * cmInPixel;
};
